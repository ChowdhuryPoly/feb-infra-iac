variable "vpc_name" {
  type        = string
  description = "This variable will be used in the VPC module as the name of it"
  default     = "feb-tf-vpc"
}


variable "cluster_name" {
  type = string
  description = "Name of the EKS cluster"
  default = "feb-tf-eks"
}

variable "backend_tag" {
  type = string
  description = "tag of the image"
  default = "latest"
}
