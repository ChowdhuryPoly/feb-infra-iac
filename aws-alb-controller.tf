resource "helm_release" "alb_controller" {
  name       = "aws-alb-controller"
  repository = "https://aws.github.io/eks-charts"
  chart      = "aws-load-balancer-controller"
  namespace  = "kube-system"

  set {
    name  = "clusterName"
    value = "feb-tf-eks"
  }
}
